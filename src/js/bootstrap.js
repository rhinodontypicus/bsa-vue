import Vue from 'vue'
const axios = require('axios');

window.Vue = Vue;
window.$http = axios.create({
    baseURL: 'http://localhost:3000',
});

require('./app');