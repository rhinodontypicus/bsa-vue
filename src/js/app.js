window.vueApp = new Vue({
    el: '#app',
    components: {
        'users': require('./components/users.vue'),
        'users-add-form': require('./components/users__add.vue')
    },
    mounted() {
        this.$nextTick(() => {
            console.log('App DOM Ready');
        });
    }
});