module.exports = {
    state: {
        users: [{id: 'fkj9h', name: 'Denis'}]
    },
    addUser(user) {
        this.state.users.push(user);
    },
    removeUser(userId) {
        let userIndex = this.state.users.map((user) => {
            return user.id;
        }).indexOf(userId);

        this.state.users.splice(userIndex, 1);
    }
};